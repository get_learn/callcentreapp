package com.callcentreagent.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.callcentreagent.model.Team;

public interface TeamRepo extends JpaRepository<Team, Long> {

}
