package com.callcentreagent.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.callcentreagent.model.Manager;

public interface ManagerRepo extends JpaRepository<Manager, Long> {

}
