package com.callcentreagent.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Team {
	@GeneratedValue
	@Id
	private Long id;
	private String teamName;
	@ManyToOne
	private Manager manager;

	public Team() {
	}

	public Team(Long id, String teamName, Manager manager) {
		this.id = id;
		this.teamName = teamName;
		this.manager = manager;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public Manager getManager() {
		return manager;
	}

	public void setManager(Manager manager) {
		this.manager = manager;
	}

}
