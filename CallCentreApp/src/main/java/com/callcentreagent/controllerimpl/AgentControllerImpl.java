package com.callcentreagent.controllerimpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.callcentreagent.controller.AgentController;
import com.callcentreagent.exceptions.UserServiceException;
import com.callcentreagent.model.Agent;
import com.callcentreagent.service.AgentService;

@RestController
public class AgentControllerImpl implements AgentController {
	@Autowired
	private AgentService agentService;

	// Returns a detail view of the specified agent with team details
	@Override
	@GetMapping(value = "agent/{Id}")
	public ResponseEntity<Object> getAgentById(@PathVariable(value = "Id") Long agentId) {
		Optional<Agent> agent = agentService.getAgentById(agentId);
		if (!agent.isPresent()) {
			throw new UserServiceException("No Data Found for AgentID" + agentId);
		}
		return new ResponseEntity<Object>(agent.get(), HttpStatus.OK);
	}

	// Creates a new Agent with the specified details
	@Override
	@PostMapping(value = "/saveAgent")
	public ResponseEntity<Object> saveAgent(@RequestBody Agent agent) {
		return new ResponseEntity<Object>(agentService.addNewAgent(agent), HttpStatus.CREATED);
	}

	// Returns a list of all agents
	@Override
	@GetMapping(value = "/agents")
	public List<Agent> getAllAgents() {
		return agentService.getAgents();
	}

}
