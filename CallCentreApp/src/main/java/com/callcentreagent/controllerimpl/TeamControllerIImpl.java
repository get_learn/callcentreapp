package com.callcentreagent.controllerimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.callcentreagent.controller.TeamController;
import com.callcentreagent.exceptions.UserServiceException;
import com.callcentreagent.model.Agent;
import com.callcentreagent.model.Team;
import com.callcentreagent.repo.AgentRepo;
import com.callcentreagent.repo.TeamRepo;
import com.callcentreagent.service.TeamService;

@RestController
public class TeamControllerIImpl implements TeamController {
	@Autowired
	private TeamService teamService;
	@Autowired
	private TeamRepo teamRepo;
	@Autowired
	private AgentRepo agentRepo;

// create new team
	@Override
	@PostMapping(value = "/createTeam")
	public ResponseEntity<Team> saveTeam(@RequestBody Team team) {
		Team team1 = teamService.saveTeam(team);
		return ResponseEntity.ok(team1);
	}

	// Assigns an agent to the specified team
	@PutMapping("/team/{id}/agent")
	public ResponseEntity<Object> assignAgent(@PathVariable(value = "id") Long teamID,
			@Valid @RequestBody Agent agentDetails) throws Exception {
		Optional<Team> team = teamRepo.findById(teamID);
		if (!team.isPresent()) {
			throw new UserServiceException("No Data Found for TeamID" + teamID);
		}
		agentDetails.setTeam(team.get());
		final Agent updatedAgent = agentRepo.save(agentDetails);
		return new ResponseEntity<Object>(updatedAgent, HttpStatus.OK);
	}

// fetch team with id
	@Override
	@GetMapping(value = "/team/{id}")
	public Team getTeamById(@PathVariable(value = "id") Long id) {
		System.out.println(id);
		return teamService.getTeamById(id);
	}

// get all teams
	@Override
	@GetMapping(value = "/teams")
	public List<Team> getAllTeams() {
		return teamService.getAllTeams();
	}

	/* teams without agents or managers */

	@GetMapping("/emptyteams")
	public List<Team> getEmptyTeams() {

		List<Team> emptyTeams = new ArrayList<Team>();

		List<Team> agentTeams = new ArrayList<Team>();

		List<Team> teams = teamRepo.findAll();
		List<Agent> agents = agentRepo.findAll();

		for (int i = 0; i < teams.size(); i++) {

			for (int c = 0; c < agents.size(); c++) {

				if (teams.get(i).getId() == agents.get(c).getTeam().getId()) {
					agentTeams.add(agents.get(c).getTeam());
				}
			}

		}
		/* after team count implementation */
		for (int i = 0; i < teams.size(); i++) {
			int countTeams = 0;
			int indexAt = -1;
			for (int c = 0; c < agentTeams.size(); c++) {

				if (teams.get(i).getId() == agentTeams.get(c).getId()) {

					countTeams++;
					indexAt = i;

				} else {
					indexAt = i;

				}
			}

			if (countTeams == 0) {
				emptyTeams.add(teams.get(indexAt));

			}

		}

		return emptyTeams;
	}

}
