package com.callcentreagent.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.callcentreagent.model.Team;

public interface TeamController {
	public ResponseEntity<Team> saveTeam(Team team);

	public Team getTeamById(Long id);

	public List<Team> getAllTeams();
}
