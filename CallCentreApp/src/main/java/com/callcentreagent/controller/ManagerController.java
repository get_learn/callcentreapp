package com.callcentreagent.controller;

import com.callcentreagent.model.Manager;

public interface ManagerController {

	public Manager addNewManager(Manager manager);
}
