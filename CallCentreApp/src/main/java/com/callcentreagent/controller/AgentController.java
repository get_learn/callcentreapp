package com.callcentreagent.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.callcentreagent.model.Agent;

public interface AgentController {

	public List<Agent> getAllAgents();

	public ResponseEntity<Object> getAgentById(Long agentid);

	public ResponseEntity<Object> saveAgent(Agent agent);
}
