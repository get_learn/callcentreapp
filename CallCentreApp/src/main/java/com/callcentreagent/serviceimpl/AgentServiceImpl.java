package com.callcentreagent.serviceimpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.callcentreagent.model.Agent;
import com.callcentreagent.repo.AgentRepo;
import com.callcentreagent.service.AgentService;

@Service
public class AgentServiceImpl implements AgentService {
	@Autowired
	private AgentRepo agentRepo;

	@Override
	public List<Agent> getAgents() {
		return agentRepo.findAll();
	}

	@Override
	public Optional<Agent> getAgentById(Long agentId) {
		return agentRepo.findById(agentId);
	}

	@Override
	public Agent addNewAgent(Agent agent) {
		return agentRepo.save(agent);
	}

	@Override
	public Agent updateAgent(Agent agent) {
		return agentRepo.save(agent);
	}

	@Override
	public void deleteAgentById(Long agentid) {
		agentRepo.deleteById(agentid);
	}

	@Override
	public void deleteAllAgents() {
		agentRepo.deleteAll();
	}

}
