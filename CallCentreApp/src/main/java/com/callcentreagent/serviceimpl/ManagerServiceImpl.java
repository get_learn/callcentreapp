package com.callcentreagent.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.callcentreagent.model.Manager;
import com.callcentreagent.repo.ManagerRepo;
import com.callcentreagent.service.ManagerService;

@Service
public class ManagerServiceImpl implements ManagerService {

	@Autowired
	private ManagerRepo managerRepo;

	@Override
	public Manager addManager(Manager manager) {
		return managerRepo.save(manager);
	}

	@Override
	public List<Manager> getAllManagers(Manager manager) {

		return managerRepo.findAll();
	}

}
