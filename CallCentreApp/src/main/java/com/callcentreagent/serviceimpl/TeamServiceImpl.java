package com.callcentreagent.serviceimpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.callcentreagent.model.Team;
import com.callcentreagent.repo.TeamRepo;
import com.callcentreagent.service.TeamService;

@Service
public class TeamServiceImpl implements TeamService {
	@Autowired
	private TeamRepo teamRepo;

	@Override
	public Team saveTeam(Team team) {
		return teamRepo.save(team);
	}

	@Override
	public Team getTeamById(Long id) {
		Optional<Team> team = teamRepo.findById(id);
		return team.get();
	}

	@Override
	public List<Team> getAllTeams() {
		return teamRepo.findAll();
	}
}
