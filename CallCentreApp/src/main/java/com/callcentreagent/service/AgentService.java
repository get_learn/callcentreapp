package com.callcentreagent.service;

import java.util.List;
import java.util.Optional;

import com.callcentreagent.model.Agent;

public interface AgentService {

	public List<Agent> getAgents();

	public Optional<Agent> getAgentById(Long agentId);

	public Agent addNewAgent(Agent agent);

	public Agent updateAgent(Agent agent);

	public void deleteAgentById(Long agentid);

	public void deleteAllAgents();
}
